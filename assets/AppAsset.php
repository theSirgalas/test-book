<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/dashbord.css',
        'popuo-box.css',
        'css/style.css',
        'css/site.css',
    ];
    public $js = [
        'js/jquery.magnific-popup.js',
        'js/jquery.polyglot.language.switcher.js',
        'js/modernizr.custom.min.js',
        'js/responsiveslides.min.js',
        'js/script.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    public function init()
    {
        $this->_includeJs();
        // $this->_includeCss();
    }

    private function _includeJs()
    {
        $controller = Yii::$app->controller->id;
        $action = Yii::$app->controller->action->id;

        if ($controller == "book" && $action == "index") {
            $this->js[] = "js/book.js";
        }
    }
}
