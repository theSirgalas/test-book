<?php

use yii\db\Migration;
use app\entities\Book;
/**
 * Class m180428_113229_add_collumn_slug_book_table
 */
class m180428_113229_add_collumn_slug_book_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(Book::tableName(),'slug',$this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(Book::tableName(),'slug');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180428_113229_add_collumn_slug_book_table cannot be reverted.\n";

        return false;
    }
    */
}
