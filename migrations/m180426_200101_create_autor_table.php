<?php

use yii\db\Migration;

/**
 * Handles the creation of table `autor`.
 */
class m180426_200101_create_autor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('autor', [
            'id' => $this->primaryKey(),
            'first_name'=>$this->string(255)->notNull(),
            'second_name'=>$this->string(255)->notNull(),
            'biograph'=>$this->text(),
            'birthday'=> $this->dateTime(),
            'day_of_death'=>$this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('autor');
    }
}
