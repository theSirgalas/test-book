<?php

use yii\db\Migration;
use app\entities\Book;
use app\entities\BookAutor;
use app\entities\BookCategory;
use app\entities\BookTag;
use app\entities\Autor;
/**
 * Class m180507_135751_insert_book
 */
class m180507_135751_insert_book extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert(Book::tableName(), [
            'title' => '451 градус по Фаренгейту',
        ]);
        $this->insert(Book::tableName(), [
            'title' => 'Марсианские хроники',
        ]);
        $this->insert(Book::tableName(), [
            'title' => 'Двухсотлетний человек',
        ]);
        $this->insert(Book::tableName(), [
            'title' => 'Мать-Земля',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete(Book::tableName());
    }

}
