<?php

use yii\db\Migration;
use app\entities\Autor;

/**
 * Class m180428_124434_add_collumn_slug_autor_table
 */
class m180428_124434_add_collumn_slug_autor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(Autor::tableName(),'slug',$this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(Autor::tableName(),'slug');
    }

}
