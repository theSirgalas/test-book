<?php

use yii\db\Migration;
use app\entities\Category;
/**
 * Class m180428_141300_insert_category_tables
 */
class m180428_141300_insert_category_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert(Category::tableName(), [
            'title' => 'рассказы',
            'description' => 'content 1',
        ]);

        $this->insert(Category::tableName(), [
            'title' => 'повести',
            'description' => 'content 1',
        ]);
        $this->insert(Category::tableName(), [
            'title' => 'Фантастика',
            'description' => 'content 1',
            'parent_id'=>1
        ]);
        $this->insert(Category::tableName(), [
            'title' => 'Фантастика',
            'description' => 'content 1',
            'parent_id'=>2
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete(Category::tableName());
    }


}
