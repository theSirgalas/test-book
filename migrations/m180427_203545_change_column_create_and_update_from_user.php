<?php

use yii\db\Migration;
use app\entities\User;
/**
 * Class m180427_203545_change_column_create_and_update_from_user
 */
class m180427_203545_change_column_create_and_update_from_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn(User::tableName(),'created_at');
        $this->dropColumn(User::tableName(),'updated_at');
        $this->addColumn(User::tableName(),'created_at',$this->dateTime());
        $this->addColumn(User::tableName(),'updated_at',$this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(User::tableName(),'created_at');
        $this->dropColumn(User::tableName(),'updated_at');
        $this->addColumn(User::tableName(),'created_at',$this->integer());
        $this->addColumn(User::tableName(),'updated_at',$this->integer());
    }

}
