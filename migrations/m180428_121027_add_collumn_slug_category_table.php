<?php

use yii\db\Migration;
use app\entities\Category;

/**
 * Class m180428_121027_add_collumn_slug_category_table
 */
class m180428_121027_add_collumn_slug_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(Category::tableName(),'slug',$this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(Category::tableName(),'slug');

        
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180428_121027_add_collumn_slug_category_table cannot be reverted.\n";

        return false;
    }
    */
}
