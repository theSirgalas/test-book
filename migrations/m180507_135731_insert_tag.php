<?php

use yii\db\Migration;
use app\entities\Tag;
use yii\db\Expression;

/**
 * Class m180507_135731_insert_tag
 */
class m180507_135731_insert_tag extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert(Tag::tableName(), [
            'title' => 'Космос',
            'created_at' => new Expression('NOW()'),
            'updated_at' => new Expression('NOW()'),
        ]);

        $this->insert(Tag::tableName(), [
            'title' => 'Роботы',
            'created_at' => new Expression('NOW()'),
            'updated_at' => new Expression('NOW()'),
        ]);
        $this->insert(Tag::tableName(), [
            'title' => 'Прочее',
            'created_at' => new Expression('NOW()'),
            'updated_at' => new Expression('NOW()'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete(Tag::tableName());
    }
}
