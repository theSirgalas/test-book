<?php

use yii\db\Migration;
use app\entities\Book;
/**
 * Class m180428_161520_add_column_image_from_book_table
 */
class m180428_161520_add_column_image_from_book_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(Book::tableName(),'image',$this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(Book::tableName(),'image');
    }

}
