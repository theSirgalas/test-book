<?php

use yii\db\Migration;
use app\entities\Autor;
/**
 * Class m180507_135720_insert_autor
 */
class m180507_135720_insert_autor extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert(Autor::tableName(), [
            'first_name' => 'Айзек',
            'second_name' => 'Азимов',
        ]);
        $this->insert(Autor::tableName(), [
            'first_name' => 'Рей',
            'second_name' => 'Бредбери',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete(Autor::tableName());
    }


}
