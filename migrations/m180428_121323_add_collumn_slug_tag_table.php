<?php

use yii\db\Migration;
use app\entities\Tag;

/**
 * Class m180428_121323_add_collumn_slug_tag_table
 */
class m180428_121323_add_collumn_slug_tag_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(Tag::tableName(),'slug',$this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn(Tag::tableName(),'slug');
    }
    
}
