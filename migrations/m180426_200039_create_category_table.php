<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category`.
 */
class m180426_200039_create_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'title'=>$this->string(255)->notNull(),
            'description'=>$this->text(),
            'parent_id'=>$this->integer()->defaultValue(null)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('category');
    }
}
