<?php

use yii\db\Migration;

/**
 * Handles the creation of table `book_autor`.
 * Has foreign keys to the tables:
 *
 * - `book`
 * - `autor`
 */
class m180426_205809_create_junction_table_for_book_and_autor_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('book_autor', [
            'book_id' => $this->integer(),
            'autor_id' => $this->integer(),
            'PRIMARY KEY(book_id, autor_id)',
        ]);

        // creates index for column `book_id`
        $this->createIndex(
            'idx-book_autor-book_id',
            'book_autor',
            'book_id'
        );

        // add foreign key for table `book`
        $this->addForeignKey(
            'fk-book_autor-book_id',
            'book_autor',
            'book_id',
            'book',
            'id',
            'CASCADE'
        );

        // creates index for column `autor_id`
        $this->createIndex(
            'idx-book_autor-autor_id',
            'book_autor',
            'autor_id'
        );

        // add foreign key for table `autor`
        $this->addForeignKey(
            'fk-book_autor-autor_id',
            'book_autor',
            'autor_id',
            'autor',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `book`
        $this->dropForeignKey(
            'fk-book_autor-book_id',
            'book_autor'
        );

        // drops index for column `book_id`
        $this->dropIndex(
            'idx-book_autor-book_id',
            'book_autor'
        );

        // drops foreign key for table `autor`
        $this->dropForeignKey(
            'fk-book_autor-autor_id',
            'book_autor'
        );

        // drops index for column `autor_id`
        $this->dropIndex(
            'idx-book_autor-autor_id',
            'book_autor'
        );

        $this->dropTable('book_autor');
    }
}
