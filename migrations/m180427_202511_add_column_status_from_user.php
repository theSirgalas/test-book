<?php

use yii\db\Migration;
use app\entities\User;

/**
 * Class m180427_202511_add_column_status_from_user
 */
class m180427_202511_add_column_status_from_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(User::tableName(),'status',$this->integer());
        $this->addColumn(User::tableName(),'access_token',$this->integer(10));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn(User::tableName(),'status');
       $this->dropColumn(User::tableName(),'access_token');
    }
    
}
