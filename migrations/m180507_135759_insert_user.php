<?php

use yii\db\Migration;
use app\entities\User;
/**
 * Class m180507_135759_insert_user
 */
class m180507_135759_insert_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert(User::tableName(), [
            'username' => 'Super',
            'email' =>  'email@email.ru'
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete(User::tableName());
    }   
}
