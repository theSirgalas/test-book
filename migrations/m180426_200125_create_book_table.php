<?php

use yii\db\Migration;

/**
 * Handles the creation of table `book`.
 */
class m180426_200125_create_book_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('book', [
            'id' => $this->primaryKey(),
            'title'=> $this->string(255)->notNull(),
            'description'=>$this->string(610),
            'content'=>$this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('book');
    }
}
