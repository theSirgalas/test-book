<?php
/**
 * @property integer $id
 * @property integer $user_id
 * @property integer $book_id
 * @property Book[] $book
 * @property User[] $user
 */
namespace app\entities\redis;

use Yii;
use app\entities\Book;
use app\entities\User;
use yii\redis\ActiveRecord;
use yii\web\NotFoundHttpException;

class TookBook extends ActiveRecord
{

    public function attributes()
    {
        return ['id',  'user_id', 'book_id'];
    }

    public function beforeSave($insert)
    {
        $this->user_id=$this->getUserId();
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQueryInterface
     */
    public function getUser(){
        return $this->hasOne(User::tableName(),['id'=>'user_id']);
    }

    /**
     * @return \yii\db\ActiveQueryInterface
     */
    public function getBook(){
        return $this->hasOne(Book::tableName(),['id'=>'book_id']);
    }

    /**
     * @return int|string
     * @throws NotFoundHttpException
     */
    private static function getUserId()
    {
        if(Yii::$app->user->isGuest)
            throw new NotFoundHttpException(Yii::t('app','User not auth'));
        return Yii::$app->user->id;
    }
}