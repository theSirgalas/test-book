<?php

namespace app\widgets\linkPager;

use yii\web\AssetBundle;

class  PagerAsset extends AssetBundle
{
    public $js = [
        'js/pager.js'
    ];
    public $css = [

    ];
    public $depends = [
        'yii\web\JqueryAsset'
    ];
    public function init()
    {
        $this->sourcePath = __DIR__ . "/assets";
        parent::init();
    }
}

