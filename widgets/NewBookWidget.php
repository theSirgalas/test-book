<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 30.04.18
 * Time: 21:46
 */

namespace app\widgets;

use app\entities\Book;
use yii\base\Widget;

class NewBookWidget extends Widget
{
    public function run()
    {
       $newBook=Book::find()->orderBy(['id'=>SORT_DESC])->with('autors','tags')->limit(12)->all();
       return $this->render('newBook',[
           'newBook'=>$newBook
       ]);
    }

}