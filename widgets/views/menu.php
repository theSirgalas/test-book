<?php
use yii\widgets\Menu;
?>
<?= Menu::widget([
    'items' => $menuItems,
    'submenuTemplate' => "\n<ul class='drop-menu' role='menu'>\n{items}\n</ul>\n",
    'options' => [
        'class' => 'nav nav-sidebar',
    ],
]) ?>