<?php

/** @var $newBook app\entities\Book */
/** @var $book app\entities\Book */

use yii\helpers\Html;
use yii\helpers\Url;

$count=1;
foreach ($newBook as $book){ ?>
<div class="col-md-4 resent-grid recommended-grid slider-first">
    <div class="resent-grid-img recommended-grid-img">
        <?= Html::a(Html::img($book->imageUrl),Url::to(['/book','id'=>$book->id])); ?>
        <div class="clck small-clck">
            <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
        </div>
        <div class="resent-grid-info recommended-grid-info">
            <h5>
                <?= Html::a($book->title,Url::to(['/book','id'=>$book->id])) ?>
            </h5>
            <div class="slid-bottom-grids">
                <div class="slid-bottom-grid">
                    <p class="author author-info">
                    <?php foreach ($book->autors as $autor){ ?>
                        <?= Html::a($autor->first_name.' '.$autor->second_name,Url::to(['autor',$autor->id]))?>
                    <?php } ?>
                    </p>
                </div>
                <div class="slid-bottom-grid slid-bottom-right">
                    <p class="views views-info">
                        <?php foreach ($book->tags as $tag){ ?>
                            <?= Html::a('#'.$tag->title, Url::to(['tag',$tag->id]))?>
                        <?php } ?>
                    </p>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>