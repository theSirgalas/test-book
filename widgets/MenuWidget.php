<?php

namespace app\widgets;

use yii\base\Widget;
use app\entities\Category;

class MenuWidget extends Widget
{

    public function run()
    {
        $categories = Category::find()->indexBy('id')->orderBy('id')->all();
       
        return $this->render('menu', [
            'menuItems' => $this->getMenuItems($categories),
        ]);
    }

    private function getMenuItems($categories, $activeId = null, $parent = null)
    {
        $menuItems = [];
        foreach ($categories as $category) {
            if ($category->parent_id === $parent) {
                $menuItems[$category->id] = [
                    'active' => $activeId === $category->id,
                    'label' => $category->title,
                    'items' => $this->getMenuItems($categories, $activeId, $category->id),
                ];
               if($category->parent_id == null){
                   $menuItems[$category->id]['url']='#';
                   $menuItems[$category->id]['options']= ['class'=>'dropdown'];
                }else{
                    $menuItems[$category->id]['url'] = ['/category/category', 'id' => $category->id];
                }
            }
        }
        return $menuItems;
    }

}