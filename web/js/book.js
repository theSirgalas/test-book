$(document).ready(function () {
    $(document).on('click','#bookAdd',function (e) {
        e.preventDefault();
        var bookID=$(this).data('id');
        $.ajax({
            type: "POST",
            url: "/book/took-book",
            data: {"book_id": bookID},
            success: function (data) {
                if(data)
                    alert(data);
                $.pjax.reload({container: "#pjax-book-button", timeout: 10000});
            }
        });
    });

    $(document).on('click','#bookRemove',function (e) {
        e.preventDefault();
        var bookID=$(this).data('id');
        $.ajax({
            type: "POST",
            url: "/book/remove-book",
            data: {"book_id": bookID},
            success: function (data) {
                $.pjax.reload({container: "#pjax-book-button", timeout: 10000});
            }
        });
    });


});