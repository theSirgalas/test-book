<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $tag app\entities\Tag; */

$this->title = $tag->title ;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recommended">
    <div class="recommended-grids english-grid">
        <div class="recommended-info">
            <div class="heading">
                <h3><?= $this->title ?></h3>
            </div>
            <div class="heading-right">
                <p><?= $tag->title ?></p>
            </div>
            <div class="clearfix"> </div>
        </div>
        <?= ListView::widget([
            'dataProvider'  => $booksDataProvider,
            'itemView'      => '_oneBook',
            'pager' => [
                'firstPageLabel' => Yii::t('app','First'),
                'lastPageLabel' => Yii::t('app','Last'),
                'prevPageLabel' => '<span class="fa fa-angle-left"></span>',
                'nextPageLabel' => '<span class="fa fa-angle-right"></span>',
            ],
        ]); ?>
        <div class="clearfix"> </div>
    </div>
</div>
