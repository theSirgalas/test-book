<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $book app\entities\Book */



$this->title = $book->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="show-top-grids">
    <div class="col-sm-12 single-left">
        <div class="song col-md-8">
            <div class="song-info">
                <h3><?= $this->title ?></h3>
            </div>
            <div class="video-grid ">
                <?= Html::img($book->imageUrl,['alt'=>$this->title]) ?>
            </div>
        </div>
        <div class="song-grid-right col-md-4">
            <div class="share">
                <h5>Took book</h5>
                <ul>
                    <li id="book">
                    <?php Pjax::begin(['options' => ['id' => 'pjax-book-button']]);
                        if(Yii::$app->user->identity->id){
                            if(!$tookBook) {
                                echo  Html::a(Yii::t('app','I took this book'),'#',['data-id'=>$book->id,'class'=>'btn btn-primary','id'=>'bookAdd']);
                            }else{
                                echo Html::a(Yii::t('app','I return the book'),'#',['data-id'=>$book->id,'class'=>'btn btn-danger','id'=>'bookRemove']);
                            }
                        }
                    Pjax::end(); ?>
                    </li>
                </ul>
            </div>
        </div>
        <div class="clearfix"> </div>
        <div class="published">
            <div class="load_more">
                <ul id="myList">
                    <li>
                        <?= $book->content; ?>
                         <div class="load-grids">
                            <div class="load-grid col-md-6">
                                <p><?=Yii::t('app','Categories'); ?>
                                <ul>
                                <?php  foreach ($book->categories as $category)
                                    echo '<li>'.Html::a($category->title, Url::to(['/category','id'=>$category->id]),['class'=>'col-md-12']).'</li>'; ?>
                                </ul>
                            </div>
                            <div class="load-grid col-md-6">
                                <p><?=Yii::t('app','Autors'); ?></p>
                                <ul>
                                <?php foreach ($book->autors as $autor)
                                    echo '<li>'.Html::a($autor->first_name.' '.$autor->second_name,Url::to(['/autor','id'=>$autor->id],['class'=>'col-md-12'])).'</li>'; ?>
                                </ul>
                            </div>
                            <div class="load-grid col-md-6">
                                <p><?=Yii::t('app','Tags'); ?></p>
                                <ul>
                                <?php foreach ($book->tags as $tag)
                                    echo '<li>'.Html::a($tag->title,Url::to(['/autor','id'=>$tag->id],['class'=>'col-md-12'])).'</li>'; ?>
                                </ul>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="clearfix"> </div>
</div>
