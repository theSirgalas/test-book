<?php

/* @var $this yii\web\View */

use app\widgets\NewBookWidget;

$this->title = 'My book';
?>
<div class="main-grids">

      <div class="recommended">
        <div class="recommended-grids">
            <div class="recommended-info">
                <h3>New Book</h3>
            </div>
            <div  id="top" class="callbacks_container">
                <?= NewBookWidget::widget(); ?>
            </div>
        <div class="clear-both"></div>
    </div>

</div>
