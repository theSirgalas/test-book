<?php

/**
 * @var $this yii\web\View
 * @var $model app\entities\Book
 * @var $form yii\widgets\ActiveForm
 * @var $autors array
 * @var $categories array
 * @var $tags array
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use kartik\select2\Select2;
?>
<div class="book-form col-md-10">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'content')->widget(\dosamigos\ckeditor\CKEditor::class, [
    'options' => ['rows' => 6],
    'preset' => 'basic'
]) ?>
<?php
if($model->image)
    echo Html::img($model->imageUrl,['width'=>200]); ?>

<?= $form->field($model, 'imageFile')->widget(FileInput::class,[
    'options' => ['accept' => 'image/*'],
]) ?>
<div class="col-md-12">
    <?= $form->field($model, 'category_id')->widget(Select2::class, [
        'data' => $categories,
        'options' => ['placeholder' =>  Yii::t('app','Select category'),'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>
</div>


<div class="col-md-12">
    <div class="col-md-6">
        <p><?= Yii::t('app','Select Autors');?></p>
        <?= $form->field($model, 'autor_id')->widget(Select2::class, [
            'data' => $autors,
            'maintainOrder' => true,
            'options' => ['placeholder' => Yii::t('app','Select autors'),'multiple' => true],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>
    </div>

</div>
<div class="col-md-12">
    <div class="col-md-6">
        <p><?= Yii::t('app','Select Tags');?></p>
        <?= $form->field($model, 'tag_id')->widget(Select2::class, [
            'data' => $tags,
            'options' => ['placeholder' => Yii::t('app','Select tag'),'multiple' => true],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>
    </div>

</div>
<div class="form-group">
    <?= Html::submitButton(Yii::t('app','Save'), ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>

</div>