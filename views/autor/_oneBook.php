<?php

/** @var $model app\entities\Book */

use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="col-md-3 resent-grid recommended-grid movie-video-grid">
    <div class="resent-grid-img recommended-grid-img">
        <?= Html::a(Html::img($model->imageUrl),Url::to(['/book','id'=>$model->id])) ?>
        <div class="clck small-clck">
            <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
        </div>
        <div class="resent-grid-info recommended-grid-info">
            <h5>
                <?= Html::a($model->title,Url::to(['/book','id'=>$model->id])) ?>
            </h5>
            <div class="slid-bottom-grids">
                <div class="slid-bottom-grid">
                    <p class="author author-info">
                        <?php foreach ($model->autors as $autor){ ?>
                            <?= Html::a($autor->first_name.' '.$autor->second_name,Url::to(['/autor','id'=>$autor->id]))?>
                        <?php } ?>
                    </p>
                </div>
                <div class="slid-bottom-grid slid-bottom-right">
                    <p class="views views-info">
                        <?php foreach ($model->tags as $tag){ ?>
                            <?= Html::a('#'.$tag->title, Url::to(['/tag','id'=>$tag->id]))?>
                        <?php } ?>
                    </p>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
</div>