<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $autor app\entities\Autor */

$this->title = $autor->first_name.' '. $autor->second_name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recommended">
    <div class="recommended-grids english-grid">
        <div class="recommended-info">
            <div class=" col-md-12 heading">
                <h3><?= $this->title ?></h3>
            </div>
            <div class="heading-right  col-md-12">
                <p class="col-md-6"><strong><?= Yii::t('app','Birthbay'); ?></strong> <?= $autor->birthday ; ?></p>
                <?php if($autor->day_of_death ){ ?>
                    <p class="col-md-6"><strong><?= Yii::t('app','Day of Dead'); ?></strong> <?= $autor->day_of_death ; ?></p>
                <?php } ?>
            </div>
            <div class="clearfix"> </div>
        </div>
        <?= ListView::widget([
            'dataProvider'  => $booksDataProvider,
            'itemView'      => '_oneBook',
            'pager' => [
            'firstPageLabel' => Yii::t('app','First'),
            'lastPageLabel' => Yii::t('app','Last'),
            'prevPageLabel' => '<span class="fa fa-angle-left"></span>',
            'nextPageLabel' => '<span class="fa fa-angle-right"></span>',
            ],
        ]); ?>
        <div class="clearfix"> </div>
    </div>
</div>