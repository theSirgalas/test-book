<?php

/**
 * @var $model app\entities\Category
 */

use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="col-md-3 resent-grid recommended-grid movie-video-grid">
    <div class="resent-grid-img recommended-grid-img">
        <h3>
            <?= Html::a( $model->title,Url::to(['/category/category','id'=>$model->id])) ?>
        </h3>
        
    </div>
    <div class="resent-grid-info recommended-grid-info recommended-grid-movie-info">
        <h5>
            <?= Html::a( $model->description,Url::to(['/category/category','id'=>$model->id])) ?>
       </h5>
    </div>
</div>