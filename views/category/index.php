<?php

/* @var $this yii\web\View */

use yii\widgets\ListView;

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recommended">
    <div class="recommended-grids english-grid">
        <div class="recommended-info">
            <div class="heading">
                <h3><?= $this->title ?></h3>
            </div>
            <div class="heading-right">
                <a  href="#small-dialog8" class="play-icon popup-with-zoom-anim">Subscribe</a>
            </div>
            <div class="clearfix"> </div>
        </div>
        <?= ListView::widget([
            'dataProvider'  => $categoresDataProvider,
            'itemView'      => '_one',
            'pager' => [
                'firstPageLabel' => Yii::t('app','First'),
                'lastPageLabel' => Yii::t('app','Last'),
                'prevPageLabel' => '<span class="fa fa-angle-left"></span>',
                'nextPageLabel' => '<span class="fa fa-angle-right"></span>',
            ],
        ]) ?>

        <div class="clearfix"> </div>
    </div>
</div>