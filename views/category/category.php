<?php

/**
 * @var $this yii\web\View
 * @var  $category app\entities\Category;
 */

use yii\widgets\ListView;
use app\widgets\linkPager\Pager;
$this->title = $category->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recommended" id="category">
    <div class="recommended-grids english-grid">
        <div class="recommended-info">
            <div class="heading">
                <h3><?= $this->title ?></h3>
            </div>
            <div class="heading-right">
                <a  href="#small-dialog8" class="play-icon popup-with-zoom-anim">Subscribe</a>
            </div>
            <div class="clearfix"> </div>
        </div>
        <?php
        if(empty($category->books)){
            foreach ($category->childs as $child){
                echo $this->render('_one',[
                    'model'=>$child
                ]);
            }
        }else{
            echo "<div class='col-xs-12 col-sm-9'>";
            echo ListView::widget([
                'dataProvider'  => $booksDataProvider,
                'itemView'      => '_oneBook',
                'summary' => false,
                'layout'=>"{items}\n</div><div class='col-xs-12 col-sm-9'>{pager}",
                'pager' => [
                    'class' => Pager::class,
                    'firstPageLabel' => Yii::t('app','First'),
                    'lastPageLabel' => Yii::t('app','Last'),
                    'maxButtonCount' => 4,
                    'options' => [
                        'class' => 'pagination',
                        'style' => 'display:inline-block;float:left;margin:10px 10px;width:auto;height:28px;'
                    ],
                    'sizeListHtmlOptions' => [
                        'class' => 'form-control',
                        'style' => 'display:inline-block;float:left;margin:10px 10px;width:auto;height:28px;'
                    ],
                    'goToPageHtmlOptions' => [
                        'class' => 'form-control',
                        'id'=>'ToPage',
                        'style' => 'display:inline-block;float:left;margin:10px 10px;width:auto;height:28px;'
                    ],
                ],
            ]);
        } ?>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>