<?php

namespace app\controllers;

use Yii;
use app\entities\Book;
use app\search\BookSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use app\entities\redis\TookBook;

/**
 * BookController implements the CRUD actions for Book model.
 */
class BookController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Book models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $book = Book::find()->where(['id'=>$id])->with('autors','tags','categories')->one();
        $tookBook = TookBook::find()->andWhere(['book_id' => $id,'user_id'=>Yii::$app->user->identity->id])->one();
        return $this->render('index', [
            'book'=>$book,
            'tookBook'=>$tookBook,
        ]);
    }

    /**
     * @throws HttpException
     */
    public function actionTookBook(){
        $id = (int) Yii::$app->request->post('book_id');
        if (Yii::$app->request->isAjax && $id > 0 ) {
            $record = TookBook::find()->andWhere(['book_id' => $id,'user_id'=>Yii::$app->user->identity->id])->one();
            $haveBook= TookBook::find()->andWhere(['user_id'=>Yii::$app->user->identity->id])->count();

            if ($record)
                throw new \RuntimeException(Yii::t('app','book already'));
                $record = new TookBook(['book_id' => $id]);
                try{
                    if(!$record->save())
                        throw new  \RuntimeException($record->errors);
                    if($haveBook)
                        return Yii::t('app','You have {haveBook} book',['haveBook' => $haveBook]);
                }
                catch (\RuntimeException $e){
                    Yii::$app->errorHandler->logException($e);
                    Yii::$app->session->setFlash('error', Yii::t('app','book not transferred'));
                }

        } else {
            throw new HttpException(404, 'The page not found.');
        }
    }

    /**
     * @throws HttpException
     * @throws \yii\db\StaleObjectException
     */
    public function actionRemoveBook(){
        $id = (int) Yii::$app->request->post('book_id');
        if (Yii::$app->request->isAjax && $id > 0 ) {
            $record = TookBook::find()->andWhere(['book_id' => $id,'user_id'=>Yii::$app->user->identity->id])->one();
            if (!$record)
                throw new \RuntimeException(Yii::t('app','book not exist'));
                try{
                    if(!$record->delete())
                        throw new  \RuntimeException($record->errors);
                }
                catch (\RuntimeException $e){
                    Yii::$app->errorHandler->logException($e);
                    Yii::$app->session->setFlash('error', $e->getMessage());
                }
        } else {
            throw new HttpException(404, 'The page not found.');
        }
    }


    
}
