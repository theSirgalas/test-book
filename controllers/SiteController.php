<?php

namespace app\controllers;

use app\entities\Book;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\forms\LoginForm;
use app\services\LoginService;
use app\forms\SignupForm;
use app\services\SignupService;
use yii\web\UploadedFile;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use app\entities\Category;
use app\entities\Tag;
use app\entities\Autor;

class SiteController extends Controller
{

    private $serviceLogin;
    private $serviceSignUp;
    

    public function __construct($id, $module, LoginService $serviceLogin, SignupService $signupService,  $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->serviceLogin = $serviceLogin;
        $this->serviceSignUp = $signupService;
    }
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $form = new LoginForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $user = $this->serviceLogin->auth($form);
                Yii::$app->user->login($user);
                return $this->goBack();
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('login', [
            'model' => $form,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    public function actionSignup()
    {
        $form = new SignupForm();

        if ($form->load(Yii::$app->request->post())&&$form->validate()) {
            try{
                $user= (new SignupService())->signup($form);
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }catch (\DomainException $ex){
                Yii::$app->session->setFlash('error', $ex->getMessage());
            }
        }
        return $this->render('signup', [
            'model' => $form,
        ]);
    }
    
    public function actionAddBook(){
        $model = new Book();
        $autors= ArrayHelper::map(Autor::find()->all(),'id','fullName');
        $tags=ArrayHelper::map(Tag::find()->asArray()->all(),'id','title');
        if ($model->load(Yii::$app->request->post())) {
            $transaction=Yii::$app->db->beginTransaction();
            try{
                $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
                if(!$model->save())
                    throw new Exception(print_r($model->errors,1));
                $transaction->commit();
                return $this->redirect(['/']);
            }catch (Exception $e){
                $transaction->rollBack();
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error',Yii::t('app','model not save'));
            }
        }
        return $this->render('add-book', [
            'model' => $model,
            'autors'=>$autors,
            'tags'=>$tags,
            'categories'=>$this->categoryArray($allCat=Category::find()->all(),null),
        ]);
    }


    /**
     * @param Category[] $categories
     * @return array
     */
    protected function categoryArray($categories,$parent){
        $returnArray=[];
        foreach ($categories as $category){
            if($category->parent_id === $parent&&$category->parent_id === null){
                $returnArray[$category->title]=$this->categoryArray($categories,$category->id);
            }
            if($category->parent_id===$parent&&$category->parent_id !== null){
                $returnArray[$category->id]=$category->title;
            }
        }
        return $returnArray;
    }

}
