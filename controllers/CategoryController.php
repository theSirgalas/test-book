<?php

namespace app\controllers;

use Yii;
use app\entities\Category;
use app\search\CategorySearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $categores=Category::find()->where(['parent_id'=>null]);
        $categoresDataProvider= new ActiveDataProvider([
            'query' => $categores,
            'pagination' => [
                'pageSize' => 24,
            ],
        ]);
        return $this->render('index', [
            'categoresDataProvider'=>$categoresDataProvider
        ]);
    }

    public function actionCategory($id){
        $page_size = Yii::$app->request->get('per-page');
        $page_size = isset( $page_size ) ? intval($page_size) : 1;
        $category=Category::find()->where(['id'=>$id])->one();
        $booksDataProvider= new ActiveDataProvider([
            'query' => $category->getBooks(),
            'pagination' => [
                'pageSize' =>  $page_size,
            ],
        ]);
        return $this->render('category',[
            'category'=>$category,
            'booksDataProvider'=>$booksDataProvider
        ]);
    }


    
}
