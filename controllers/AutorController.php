<?php

namespace app\controllers;

use Yii;
use app\entities\Autor;
use app\search\AutorSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AutorController implements the CRUD actions for Autor model.
 */
class AutorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Autor models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $autors=Autor::find()->where(['id'=>$id])->one();
        $booksDataProvider= new ActiveDataProvider([
            'query' => $autors->getBooks(),
            'pagination' => [
                'pageSize' => 24,
            ],
        ]);
        return $this->render('index',[
            'autor'=>$autors,
            'booksDataProvider'=>$booksDataProvider
        ]);
    }

   
}
