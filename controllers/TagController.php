<?php

namespace app\controllers;

use Yii;
use app\entities\Tag;
use app\search\TagSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TagController implements the CRUD actions for Tag model.
 */
class TagController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionIndex($id)
    {
        $tag=Tag::find()->where(['id'=>$id])->one();
        $booksDataProvider= new ActiveDataProvider([
            'query' => $tag->getBooks(),
            'pagination' => [
                'pageSize' => 24,
            ],
        ]);
        return $this->render('index',[
            'tag'=>$tag,
            'booksDataProvider'=>$booksDataProvider
        ]);
    }

}
