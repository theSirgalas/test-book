<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\search\AutorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','Autors');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="autor-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app','Create Autor'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute'=>'first_name',
                'format'=>'raw',
                'filter' => Select2::widget([
                    'name' => 'CategorySearch[parent]',
                    'data' => $firstNameAutors,
                    'size' => Select2::MEDIUM,
                    'options' => ['placeholder' => 'Select a first name'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])
            ],    
            [
                'attribute'=>'second_name',
                'format'=>'raw',
                'filter' => Select2::widget([
                    'name' => 'CategorySearch[parent]',
                    'data' => $secondNameAutors,
                    'size' => Select2::MEDIUM,
                    'options' => ['placeholder' => 'Select a second name'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])
            ],
            'birthday:datetime',
            'day_of_death:datetime',
            
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
