<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\entities\Autor */

$this->title = Yii::t('app','Update Autor').':'. $model->first_name.' '.$model->second_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Autors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app','Update');
?>
<div class="autor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
