<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\entities\Book */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Books', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value'=> function($model){
                    return Html::img($model->imageUrl,['width'=>200]);
                }
            ],
            'description',
            'content:ntext',
            'slug',
            [
                'attribute'=>'autors',
                'format'=>'raw',
                'label'=>Yii::t('app','Autors'),
                'value'=>function()use($model){
                    $result='';
                    if(!empty($model->autors)) {
                        foreach ($model->autors as $autor) {
                            $result .= "<p>" . $autor->first_name . ' ' . $autor->second_name . '</p>';
                        }
                    }
                    return $result;
                }
            ],
            [
                'attribute'=>'Categoty',
                'format'=>'raw',
                'label'=>Yii::t('app','Categoty'),
                'value'=>function()use($model){
                    $result='';
                    if(!empty($model->categories)) {
                        foreach ($model->categories as $category) {
                            $result .= "<p>" . $category->title .'</p>';
                        }
                    }
                    return $result;
                }
            ],
            [
                'attribute'=>'Tags',
                'format'=>'raw',
                'label'=>Yii::t('app','Tags'),
                'value'=>function()use($model){
                    $result='';
                    if(!empty($model->categories)) {
                        foreach ($model->tags as $tag) {
                            $result .= "<p>" . $tag->title .'</p>';
                        }
                    }
                    return $result;
                }
            ]
        ],
    ]) ?>

</div>
