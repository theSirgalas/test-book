<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\entities\Book */

$this->title = Yii::t('app','Update Book').': '.$model->title;
$this->params['breadcrumbs'][] = ['label' => 'Books', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="book-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'autors' => $autors,
        'categories'=>$categories,
        'tags'=>$tags
    ]) ?>

</div>
