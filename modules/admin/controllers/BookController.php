<?php

namespace app\modules\admin\controllers;

use app\entities\Autor;
use app\entities\Category;
use app\entities\Tag;
use Yii;
use app\entities\Book;
use app\search\BookSearch;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * BookController implements the CRUD actions for Book model.
 */
class BookController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Book models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BookSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Book model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Book model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Book();
        $autors= ArrayHelper::map(Autor::find()->all(),'id','fullName');
        $tags=ArrayHelper::map(Tag::find()->asArray()->all(),'id','title');
        if ($model->load(Yii::$app->request->post())) {
            $transaction=Yii::$app->db->beginTransaction();
            try{
                $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
                if(!$model->save())
                    throw new Exception(print_r($model->errors,1));
                $transaction->commit();
                return $this->redirect(['view', 'id' => $model->id]);
            }catch (Exception $e){
                $transaction->rollBack();
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error',Yii::t('app','model not save'));
            }
        }
        return $this->render('create', [
            'model' => $model,
            'autors' => $autors,
            'categories'=>$this->categoryArray($allCat=Category::find()->all(),null),
            'tags'=>$tags
        ]);
    }

    /**
     * Updates an existing Book model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $autors= ArrayHelper::map(Autor::find()->all(),'id','fullName');
        $tags=ArrayHelper::map(Tag::find()->asArray()->all(),'id','title');
        if ($model->load(Yii::$app->request->post())) {
            $transaction=Yii::$app->db->beginTransaction();
            try{
                $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
                if(!$model->save())
                    throw new Exception(print_r($model->errors,1));
                $transaction->commit();
                return $this->redirect(['view', 'id' => $model->id]);
            }catch (Exception $e){
                $transaction->rollBack();
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error',Yii::t('app','model not save'));
            }
        }
        return $this->render('update', [
            'model' => $model,
            'autors' => $autors,
            'categories'=>$this->categoryArray($allCat=Category::find()->all(),null),
            'tags'=>$tags
        ]);
    }

    /**
     * Deletes an existing Book model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Book model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Book the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Book::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param Category[] $categories
     * @return array
     */
    protected function categoryArray($categories,$parent){
        $returnArray=[];
        foreach ($categories as $category){
                if($category->parent_id === $parent&&$category->parent_id === null){
                    $returnArray[$category->title]=$this->categoryArray($categories,$category->id);
                }
               if($category->parent_id===$parent&&$category->parent_id !== null){
                   $returnArray[$category->id]=$category->title;
                }
            }
        return $returnArray;
    }
}
