<?php

namespace app\components\rules;




use app\entities\Tag;
use Yii;
use \yii\web\UrlRule;

class TagUrlRule extends UrlRule
{

    public $pattern = 'tag';
    public $route = 'tag';

    public function createUrl($manager, $route, $params)
    {
        if ($route == 'tag' && !empty($params['id'])) {

                $tag= Tag::find()->where(['id' => $params['id']])->one();
                if (!$tag) {
                    return false;
                }

                $url = 'tag/' . $tag->slug;

            $param = '';
            if (!empty($params['filter'])) {
                foreach ($params['filter'] as $k => $v) {
                    $param .= ($param == '' ? '?' : '&') . $k . "=" . $v;
                }
            }
            return $url . $param;
        }
        return false;
    }

    public function parseRequest($manager, $request)
    {
        if(strpos($request->pathInfo,'tag/')==0){

            $tagSlug=array_pop(explode('/',$request->pathInfo));

            $category = Tag::find()->where(['slug'=>$tagSlug])->one();

            if ($category) {
                return ['/tag', ['id' => $category->id]];
            }
        }
        return false;
    }

}
