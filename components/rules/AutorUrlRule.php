<?php

namespace app\components\rules;



use app\entities\Autor;
use Yii;
use \yii\web\UrlRule;

class AutorUrlRule extends UrlRule
{

    public $pattern = 'autor';
    public $route = 'autor';

    public function createUrl($manager, $route, $params)
    {
        if ($route == 'autor' && !empty($params['id'])) {

                $autor= Autor::find()->where(['id' => $params['id']])->one();
                if (!$autor) {
                    return false;
                }

                $url = '/autor/' . $autor->slug;

            $param = '';
            if (!empty($params['filter'])) {
                foreach ($params['filter'] as $k => $v) {
                    $param .= ($param == '' ? '?' : '&') . $k . "=" . $v;
                }
            }
            return $url . $param;
        }
        return false;
    }

    public function parseRequest($manager, $request)
    {
        if(strpos($request->pathInfo,'autor/')==0){

            $categorySlug=array_pop(explode('/',$request->pathInfo));

            $category = Autor::find()->where(['slug'=>$categorySlug])->one();

            if ($category) {
                return ['/autor', ['id' => $category->id]];
            }
        }
        return false;
    }

}
