<?php

namespace app\components\rules;



use app\entities\Autor;
use app\entities\Book;
use Yii;
use \yii\web\UrlRule;

class BookUrlRule extends UrlRule
{

    public $pattern = 'book';
    public $route = 'book';

    public function createUrl($manager, $route, $params)
    {
        if ($route == 'book' && !empty($params['id'])) {

                $book= Book::find()->where(['id' => $params['id']])->one();
                if (!$book) {
                    return false;
                }

                $url = '/book/' . $book->slug;

            $param = '';
            if (!empty($params['filter'])) {
                foreach ($params['filter'] as $k => $v) {
                    $param .= ($param == '' ? '?' : '&') . $k . "=" . $v;
                }
            }
            return $url . $param;
        }
        return false;
    }

    public function parseRequest($manager, $request)
    {
        
        if(strpos($request->pathInfo,'/book')==0){

            $bookSlug=array_pop(explode('/',$request->pathInfo));

            $category = Book::find()->where(['slug'=>$bookSlug])->one();

            if ($category) {
                return ['/book', ['id' => $category->id]];
            }
        }
        return false;
    }

}
