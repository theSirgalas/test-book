<?php

namespace app\components\rules;


use app\entities\Category;
use Yii;
use \yii\web\UrlRule;

class CategoryUrlRule extends UrlRule
{

    public $pattern = 'catalog';
    public $route = 'category';

    public function createUrl($manager, $route, $params)
    {
        if ($route == 'category/category' && !empty($params['id'])) {

                $category = Category::find()->where(['id' => $params['id']])->one();
                if (!$category) {
                    return false;
                }
                $url = '/category/' . $category->slug;

            $param = '';
            if (!empty($params['page'])&&!empty($params['per-page'])) {
                $param = '?page=' . $params['page'] . '&per-page=' . $params['per-page'];
            }

            return $url . $param;
        }

        return false;
    }

    public function parseRequest($manager, $request)
    {
        if(strpos($request->pathInfo,'category/')==0){

            $categorySlug=array_pop(explode('/',$request->pathInfo));

            $category = Category::find()->where(['slug'=>$categorySlug])->one();

            if ($category) {
                return ['/category/category', ['id' => $category->id]];
            }
        }
        return false;
    }

}
